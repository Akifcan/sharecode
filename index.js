import express from 'express'
import http from 'http'
import exphbs from 'express-handlebars'
import { Server } from 'socket.io'

const app = express()
const server = http.createServer(app)
const io = new Server(server)

const PORT = process.env.PORT || 3000

const hbs = exphbs.create({});
app.engine('handlebars', hbs.engine)
app.set('view engine', 'handlebars')
app.use(express.static('public'))

const rooms = []
app.get('/choose-theme', function (req, res, next) {
    res.render('choose-theme');
})

app.get('/', function (req, res, next) {
    res.render('home');
})

app.get('/attendee/:roomId/:username', (req, res) => {
    res.render('attendee')
})

app.get('/session-end', (req, res) => {
    res.render('session-end')
})

app.post('/room/:id', (req, res) => {
    const currentRoom = rooms.find(room => room.roomId === parseInt(req.params.id))
    res.json(currentRoom)
})


io.on('connection', (socket) => {
    socket.on('create-room', ({ username }) => {
        const roomId = Math.floor(Math.random() * 9999) + 1000
        rooms.push({
            roomId,
            files: [],
            users: [
                {
                    username,
                    instructor: true
                }
            ]
        })
        socket.join(roomId)
        console.log(username + ' joined ' + roomId);

        socket.emit('get-current-room', { roomId, users: rooms.find(room => room.roomId === roomId) })

        socket.on('disconnect', () => io.to(roomId).emit('host-leaved'))

        socket.on('new-file', ({ fileName, currentLanguage, content }) => {
            const currentRoom = rooms.find(room => room.roomId === roomId)
            const checkFile = currentRoom.files.find(file => file.fileName === fileName)
            if (!checkFile) {
                currentRoom.files.push({ fileName, currentLanguage, content })
            } else {
                checkFile.content = content
            }
            io.to(roomId).emit('list-files', currentRoom)
        })

        socket.on('delete-file', fileName => {
            console.log('filename' + fileName);
            const currentRoom = rooms.find(room => room.roomId === roomId)
            const checkFile = currentRoom.files.find(file => file.fileName === fileName)
            console.log(checkFile);
            if (checkFile) {
                currentRoom.files = currentRoom.files.filter(file => file.fileName !== fileName)
            }
            // console.log(currentRoom.files);
            io.to(roomId).emit('list-files', currentRoom)
        })

    })


    socket.on('join-room', ({ username, roomId }) => {
        const currentRoom = rooms.find(room => room.roomId === parseInt(roomId))
        if (currentRoom) {
            socket.join(parseInt(roomId))
            currentRoom.users.push({
                username,
                instructor: false
            })
            socket.emit('get-current-room', { roomId: parseInt(roomId), users: rooms.find(room => room.roomId === parseInt(roomId)) })
            socket.broadcast.to(parseInt(roomId)).emit('an-user-joined', {
                username,
                instructor: false
            })
            socket.on('disconnect', () => {
                const userIndex = currentRoom.users.findIndex(user => username === username)
                currentRoom.users.splice(userIndex, 1)
                io.to(parseInt(roomId)).emit('an-user-leaved', currentRoom)
            })
        } else {
            socket.emit('room-not-found')
        }
    })
});

server.listen(PORT, () => {
    console.log(`Working on ${PORT}`);
})