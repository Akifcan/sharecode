export const joinRoom = 'join-room'
export const createRoom = 'create-room'
export const getCurrentRoom = 'get-current-room'
export const roomNotFound = 'room-not-found'
export const anUserJoined = 'an-user-joined'
export const hostLeaved = 'host-leaved'
export const anUserLeaved = 'an-user-leaved'
export const newFile = 'new-file'
export const listFiles = 'list-files'

export const listInitialUsers = (users) => {
    const userList = document.querySelector('.attendee-list')
    userList.innerHTML = ''
    users.forEach(user => {
        const li = document.createElement('li')
        const avatar = document.createElement('avatar')
        const username = document.createElement('p')

        avatar.className = `avatar ${user.instructor && 'instructor'}`
        avatar.textContent = user.username.substr(0, 2)

        username.textContent = user.username
        li.appendChild(avatar)
        li.appendChild(username)
        userList.appendChild(li)
    })
}

export const listCreatedFiles = (files, editor, instructor = false) => {
    const fileList = document.querySelector('.file-names')
    fileList.innerHTML = ''
    files.forEach(file => {
        const li = document.createElement('li')
        const p = document.createElement('p')

        p.innerHTML = `${file.fileName}.${file.currentLanguage.extension}`

        li.addEventListener('click', _ => {
            editor.setOption('mode', file.currentLanguage.name)
            editor.setValue(file.content)
        })

        li.appendChild(p)
        if (instructor) {
            const button = document.createElement('button')
            button.setAttribute('filename', file.fileName)
            button.className = 'remove'
            button.textContent = '❌'
            button.setAttribute('title', 'remove')
            li.appendChild(button)
        }
        fileList.appendChild(li)
    })
}

export const addNewUserToList = (user) => {
    const userList = document.querySelector('.attendee-list')
    const li = document.createElement('li')
    const avatar = document.createElement('avatar')
    const username = document.createElement('p')

    avatar.className = `avatar ${user.instructor && 'instructor'}`
    avatar.textContent = user.username.substr(0, 2)

    username.textContent = user.username
    li.appendChild(avatar)
    li.appendChild(username)
    Snackbar.show({ text: `${user.username} joined`, pos: 'bottom-right' });
    userList.appendChild(li)
}

export const setRoomId = (roomId) => {
    document.querySelector('.room-info b').textContent = roomId
}

export const getJoinedUserInformation = () => {
    return {
        roomId: window.location.href.split('/')[4],
        username: window.location.href.split('/')[5]
    }
}

