const REDIRECT_PATH = '/'

if (localStorage.getItem('theme')) {
    window.location.href = REDIRECT_PATH
}

document.querySelectorAll('.box').forEach(item => {
    item.addEventListener('click', _ => {
        localStorage.setItem('theme', item.dataset.theme)
        window.location.href = REDIRECT_PATH
    })
})