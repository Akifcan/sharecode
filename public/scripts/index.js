import { hostLeaved, getJoinedUserInformation, joinRoom, roomNotFound, listCreatedFiles, listFiles, newFile, anUserLeaved, createRoom, getCurrentRoom, listInitialUsers, setRoomId, anUserJoined, addNewUserToList } from "./socketEvents.js";
const socket = io();
const codeTextarea = document.getElementById('code')
const editor = CodeMirror.fromTextArea(codeTextarea, {
    lineNumbers: true,
    autoCorrect: true,
    autoComplete: true,
    mode: 'javascript',
    smartIndent: true,
    theme: localStorage.theme === 'dark' ? 'juejin' : '3024-night',
    value: 'Paste your code here',
    screenReaderLabel: 'Copy your code here'
});
editor.setValue('\t \t \t Click created files and copy your code')



socket.on(anUserJoined, ({ username, instructor }) => {
    addNewUserToList({ username, instructor })
})

socket.on(anUserLeaved, (users) => {
    listInitialUsers(users.users)
})

socket.on(listFiles, room => {
    listCreatedFiles(room.files, editor)
})

socket.on(hostLeaved, () => window.location.href = '/session-end')

const { roomId, username } = getJoinedUserInformation()

socket.emit(joinRoom, { username: username.replace('%20', ' '), roomId })

socket.on(roomNotFound, () => {
    alert(`We can't found room with this id. Sorry.`)
})

socket.on(getCurrentRoom, ({ roomId, users }) => {
    listInitialUsers(users.users)
    listCreatedFiles(users.files, editor)
    setRoomId(roomId)
    document.querySelector('.container--instructor').classList.remove('d-none')
})

