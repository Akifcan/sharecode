import { listCreatedFiles, listFiles, newFile, anUserLeaved, createRoom, getCurrentRoom, listInitialUsers, setRoomId, anUserJoined, addNewUserToList } from "./socketEvents.js";
const socket = io();

document.querySelector('body').classList.add(localStorage.theme || 'light')

const codeTextarea = document.getElementById('code')
const editor = CodeMirror.fromTextArea(codeTextarea, {
    lineNumbers: true,
    autoCorrect: true,
    autoComplete: true,
    mode: 'javascript',
    smartIndent: true,
    theme: localStorage.theme === 'dark' ? 'juejin' : '3024-night',
    value: 'Paste your code here',
    screenReaderLabel: 'Paste your code here'
});
editor.setValue('\t \t \t Paste your code here')

socket.on(anUserJoined, ({ username, instructor }) => {
    addNewUserToList({ username, instructor })
})

socket.on(anUserLeaved, (users) => {
    listInitialUsers(users.users)
})

socket.on(listFiles, room => {
    listCreatedFiles(room.files, editor, true)
    console.log(document.querySelectorAll('.remove'));
    document.querySelectorAll('.remove').forEach(item => {
        item.addEventListener('click', _ => {
            socket.emit('delete-file', item.getAttribute('filename'))
        })
    })
})


function checkTheme() {
    if (!localStorage.getItem('theme')) {
        window.location.href = '/choose-theme'
    }
}

checkTheme()

const userInfo = {}

document.getElementById('get-username').addEventListener('click', _ => {
    const username = document.getElementById('username').value
    if (username.trim().length < 3) {
        alert('Please enter at least 3 character for name')
        return
    }
    userInfo.name = username
    const activeWizard = document.querySelector('.wizard.active')
    const nextWizard = document.querySelector('.wizard.second')
    activeWizard.classList.remove('active')
    activeWizard.setAttribute('aria-expanded', "false")

    nextWizard.classList.add('active')
    nextWizard.setAttribute('aria-expanded', "true")
})

document.getElementById('join').addEventListener('click', _ => {
    const roomId = document.getElementById('roomId').value
    if (roomId.trim().length < 0) {
        alert('Please enter a room id to join')
        return
    }
    window.location.href = `/attendee/${roomId}/${userInfo.name}`
    // socket.emit(joinRoom, { username: userInfo.name, roomId })
})

document.getElementById('create-room').addEventListener('click', _ => {
    socket.emit(createRoom, { username: userInfo.name, })
    socket.on(getCurrentRoom, ({ roomId, users }) => {
        listInitialUsers(users.users)
        setRoomId(roomId)
        document.querySelector('.container--join').classList.add('d-none')
        document.querySelector('.container--instructor').classList.remove('d-none')
    })
})




const languages = [
    {
        name: 'css',
        extension: 'css'
    },
    {
        name: 'dart',
        extension: 'dart'
    },
    {
        name: 'java',
        extension: 'java'
    },
    {
        name: 'javascript',
        extension: 'js'
    },
    {
        name: 'pyhton',
        extension: 'py'
    },
    {
        name: 'sql',
        extension: 'sql'
    },
    {
        name: 'typescript',
        extension: 'ts'
    },
]

let currentLanguage = languages[3]


const currentMode = document.querySelector('.current-mode')
const modeWindow = document.querySelector('.select-mode')

document.querySelector('.mode').addEventListener('click', _ => {
    modeWindow.classList.toggle('active')
})


const modes = document.querySelector('.select-mode ul')

languages.forEach(language => {
    const mode = document.createElement('li')

    mode.addEventListener('click', _ => {
        currentLanguage = language
        modeWindow.classList.remove('active')
        currentMode.querySelector('img').setAttribute('src', `/images/languages/${language.name}.png`)
        currentMode.querySelector('img').setAttribute('alt', language.name)
        currentMode.querySelector('b').textContent = language.name
        editor.setOption('mode', language.name)
        Snackbar.show({ text: `Mode switched with ${language.name}`, pos: 'bottom-right' });
    })

    mode.innerHTML = `<p>${language.name}</p>`
    modes.appendChild(mode)
})

document.querySelector('.save-code').addEventListener('click', _ => {
    document.querySelector('.create-file').classList.toggle('active')
})

document.querySelector('.create-file').addEventListener('submit', e => {
    e.preventDefault()
    const fileName = document.getElementById('file-name').value
    document.querySelector('.create-file').classList.remove('active')
    socket.emit(newFile, { fileName, currentLanguage, content: editor.getValue() })
    editor.setValue('\t \t')
})

document.getElementById('back').addEventListener('click', _ => {
    document.querySelector('.create-file').classList.remove('active')
})